<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Groups_model extends CI_Model {

	public function get( $group_id = false )
	{

		if ( $group_id == false )
		{
			$q = $this->db->get( 'groups' );
			$q = $q->result();
		}
		else
		{
			$this->db->where( 'group_id' , $group_id );
			$q = $this->db->get( 'groups' );
			$q = $q->row();
		}

		return $q;

	}

	public function update( $group )
	{
		$this->db->where( 'group_id' , $group['group_id'] );
		$this->db->update( 'groups' , $group );
	}

	public function create( $group )
	{
		$this->db->insert( 'groups' , $group );
	}

	public function delete( $group )
	{
		$this->db->where( 'group_id' , $group['group_id'] );
		$this->db->delete( 'groups' );
	}



}