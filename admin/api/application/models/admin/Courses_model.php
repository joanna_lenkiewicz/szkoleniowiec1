<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Courses_model extends CI_Model {

	public function get( $course_id = false )
	{

		if ( $course_id == false )
		{
			$q = $this->db->get( 'courses' );
			$q = $q->result();
		}
		else
		{
			$this->db->where( 'course_id' , $course_id );
			$q = $this->db->get( 'courses' );
			$q = $q->row();
		}

		return $q;

	}

	public function update( $course )
	{
		$this->db->where( 'course_id' , $course['course_id'] );
		$this->db->update( 'courses' , $course );
	}

	public function create( $course )
	{
		$this->db->insert( 'courses' , $course );
	}

	public function delete( $course )
	{
		$this->db->where( 'course_id' , $course['course_id'] );
		$this->db->delete( 'courses' );
	}



}