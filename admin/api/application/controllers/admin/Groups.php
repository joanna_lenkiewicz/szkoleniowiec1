<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Groups extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$post = file_get_contents( 'php://input' );
		$_POST = json_decode( $post , true );

		$this->load->model( 'admin/Groups_model' );

	}

	public function get( $group_id = false )
	{	
		$output = $this->Groups_model->get( $group_id );
		echo json_encode( $output );	
	}


	public function update()
	{
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('nazwa','Nazwa grupy','required|min_length[2]');
		$this->form_validation->set_rules('organizacja','Nazwa organizacji','required|min_length[2]');
		$this->form_validation->set_rules('opis','Opis','required|min_length[2]');

		if ($this->form_validation->run()) {
			$group = $this->input->post( 'group' );
			$this->Groups_model->update( $group );
		} else {
			$errors['nazwa'] = form_error('nazwa');
			$errors['organizacja'] = form_error('organizacja');
			$errors['opis'] = form_error('opis');		
			echo json_encode($errors);
		}
	}

	public function create()
	{
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('nazwa','Nazwa grupy','required|min_length[2]');
		$this->form_validation->set_rules('organizacja','Nazwa organizacji','required|min_length[2]');
		$this->form_validation->set_rules('opis','Opis','required|min_length[2]');

		if ($this->form_validation->run()) {
			$group = $this->input->post( 'group' );			
			$this->Groups_model->create( $group );
		} else {
			$errors['nazwa'] = form_error('nazwa');
			$errors['organizacja'] = form_error('organizacja');
			$errors['opis'] = form_error('opis');
			echo json_encode($errors);
		}

	}

	public function delete()
	{
		$group = $this->input->post( 'group' );
		$this->Groups_model->delete( $group );
	}

}
