<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Courses extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$post = file_get_contents( 'php://input' );
		$_POST = json_decode( $post , true );

		$this->load->model( 'admin/Courses_model' );

	}

	public function get( $course_id = false )
	{	
		$output = $this->Courses_model->get( $course_id );
		echo json_encode( $output );	
	}


	public function update()
	{
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('nazwa','Nazwa kursu','required|min_length[3]');
		$this->form_validation->set_rules('opis','Opis','required|min_length[2]');

		if ($this->form_validation->run()) {
			$course = $this->input->post( 'course' );
			$this->Courses_model->update( $course );
		} else {
			$errors['nazwa'] = form_error('nazwa');
			$errors['opis'] = form_error('opis');		
			echo json_encode($errors);
		}
	}

	public function create()
	{
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('nazwa','Nazwa kursu','required|min_length[3]');
		$this->form_validation->set_rules('opis','Opis','required|min_length[2]');

		if ($this->form_validation->run()) {
			$course = $this->input->post( 'course' );			
			$this->Courses_model->create( $course );
		} else {
			$errors['nazwa'] = form_error('nazwa');
			$errors['opis'] = form_error('opis');
			echo json_encode($errors);
		}

	}

	public function delete()
	{
		$course = $this->input->post( 'course' );
		$this->Courses_model->delete( $course );
	}

}
