<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$post = file_get_contents( 'php://input' );
		$_POST = json_decode( $post , true );

		$this->load->model( 'admin/Users_model' );

	}

	public function get( $user_id = false )
	{	
		// $token = $this->input->post( 'token' );
		// $token = $this->jwt->decode( $token , config_item( 'encryption_key' ) );
		// if ( $token->role != 'admin' )
		// 	exit( 'Nie jesteś adminem' );
		$output = $this->Users_model->get( $user_id );
		echo json_encode( $output );	
	}

	public function getZal()
	{	
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('kurs1','kurs1.user_id=users.user_id');
        $this->db->where('kurs1.status', 1); 
        $query = $this->db->get();
        $data=$query->result_array();
		echo json_encode( $data );	
	}

	public function getPass()
	{	
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('kurs1','kurs1.user_id=users.user_id');
        $this->db->where('kurs1.status', 0); 
        $query = $this->db->get();
        $data=$query->result_array();
		echo json_encode( $data );	
	}

	public function getNoStart()
	{	
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('kurs1','kurs1.user_id=users.user_id');
        $this->db->where('kurs1.status', 2); 
        $query = $this->db->get();
        $data=$query->result_array();
		echo json_encode( $data );	
	}

	public function usersLog()
	{	
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('user_lastaccess','user_lastaccess.user_id=users.user_id');
        $query = $this->db->get();
        $data=$query->result_array();
		echo json_encode( $data );
	}

	public function resultCourse()
	{	
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('wyniki','wyniki.user_id=users.user_id');
        $query = $this->db->get();
        $data=$query->result_array();
		echo json_encode( $data );
	}

	public function problem()
	{	
        $query = $this->db->get( 'zgloszenia' );
		$data = $query->result();
		echo json_encode( $data );
	}

	public function statUsers()
	{	
        $query = $this->db->count_all('kurs1');
		echo $query;
	}

	public function statUsersZal()
	{	
        //$query = $this->db->query("SELECT status FROM kurs1 where 'status'=1;");
        $query = $this->db->query("SELECT status FROM kurs1 WHERE status='1'");
        echo $query->num_rows();
	}

	public function statUsersNo()
	{	
        $query = $this->db->query("SELECT status FROM kurs1 WHERE status='0'");
        echo $query->num_rows();
	}

	public function statUsersNoStart()
	{	
        $query = $this->db->query("SELECT status FROM kurs1 WHERE status='2'");
        echo $query->num_rows();
	}

	public function update()
	{
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('username','Nazwa użytkownika','required|min_length[4]');
		$this->form_validation->set_rules('password','Nowe hasło','required|min_length[8]');
		$this->form_validation->set_rules('firstname','Imię','required|min_length[3]');
		$this->form_validation->set_rules('lastname','Nazwisko','required|min_length[3]');
		$this->form_validation->set_rules('email','E-mail','required|valid_email|callback_unique_email');
		$this->form_validation->set_rules('firma','Organizacja','required|min_length[2]');
		

		if ($this->form_validation->run()) {
			$user = $this->input->post( 'user' );

			$user['password'] = crypt($user['password'], config_item('encryption_key'));

			$this->Users_model->update( $user );
		} else {
			$errors['username'] = form_error('username');
			$errors['password'] = form_error('password');
			$errors['firstname'] = form_error('firstname');
			$errors['lastname'] = form_error('lastname');
			$errors['email'] = form_error('email');
			$errors['firma'] = form_error('firma');
			
			echo json_encode($errors);
		}
	}

	public function create()
	{
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('username','Nazwa użytkownika','required|min_length[4]');
		$this->form_validation->set_rules('password','Nowe hasło','required|min_length[8]');
		$this->form_validation->set_rules('firstname','Imię','required|min_length[3]');
		$this->form_validation->set_rules('lastname','Nazwisko','required|min_length[3]');
		$this->form_validation->set_rules('email','E-mail','required|valid_email|callback_unique_email');
		$this->form_validation->set_rules('firma','Organizacja','required|min_length[2]');

		if ($this->form_validation->run()) {
			$user = $this->input->post( 'user' );
			
			$user['password'] = crypt($user['password'], config_item('encryption_key'));
			
			$this->Users_model->create( $user );
		} else {
			$errors['username'] = form_error('username');
			$errors['password'] = form_error('password');
			$errors['firstname'] = form_error('firstname');
			$errors['lastname'] = form_error('lastname');
			$errors['email'] = form_error('email');
			$errors['firma'] = form_error('firma');

			echo json_encode($errors);
		}

	}

	public function delete()
	{
		$user = $this->input->post( 'user' );
		$this->Users_model->delete( $user );
	}


	function unique_email() 
	{
		$user_id = $this->input->post('user_id');
		$email = $this->input->post('email');

		if ($this->Users_model->get_unique($user_id, $email))
		{
			$this->form_validation->set_message('unique_email','Inny użytkownik ma taki email');
			return false;
		} else {
			return true;
		}
	}

	public function login()
	{
		$adminname = $this->input->post( 'adminname' );
		$password = $this->input->post( 'password' );
		$password = crypt( $password , config_item( 'encryption_key' ) );

		$login = $this->Users_model->login( $adminname , $password );

		if ( !$login )
		{
			$output['error'] = 'Błędne hasło lub email';
		}
		else
		{
			$token = $this->jwt->encode( array(
				'admin_id' => $login->admin_id,
				'adminname' => $login->adminname,
				'email' => $login->email,
				'role' => $login->role
				) , config_item( 'encryption_key' ) );

			$output['token'] = $token;
		}

		echo json_encode( $output );

	}


}
