'use strict';

var myCtrls = angular.module( 'myCtrls' , [ 'ngRoute' ] );

//Menu
myCtrls.controller('navigation', ['$scope','$location','checkToken','jwtHelper', function($scope,$location,checkToken,jwtHelper){

		
		$scope.navigation = function() {
				window.location.href = 'login.html';
			};

		$scope.loginOn = function() {
				window.location.href = 'index.html';
			};
		
		//$scope.loggedIn= false;

		if (checkToken.loggedIn()) {
				$scope.loggedIn=true
			} else {
				$scope.loggedIn=false
			};

		$scope.logout = function() {
				checkToken.del();
				location.reload();
				console.log("Wylogowano.");
			};		      
}]);

// ---------------------- UŻYTKOWNICY  ----------------------------------------------------------------

// Wyświetlanie użytkowników
myCtrls.controller('users', ['$scope', '$http','checkToken', function($scope, $http, checkToken){		


			$http.post('api/admin/users/get', {
				token: checkToken.raw()
			}).success(function(data) {
				$scope.users = data;
			}).	error(function(data){
				console.log("Błąd komunikacji z API");
			});


			$scope.delete = function(user, $index) {
				$scope.users.splice($index , 1);
				$http.post( 'api/admin/users/delete/' , {
					user : user
				}).success(function() {
					console.log(user);
				}).error( function(){
				console.log( 'Błąd komunikacji z API.' );
				});
			};

}]);

// Wyświetlanie użytkowników - data ostatniego logowania
myCtrls.controller('usersLog', ['$scope', '$http','checkToken', function($scope, $http, checkToken){		

			$http.post('api/admin/users/usersLog', {
				token: checkToken.raw()
			}).success(function(data) {
				console.log("Dane pobrane.");
				$scope.users = data;
			}).	error(function(data){
				console.log("Błąd komunikacji z API");
			});


}]);

// Wyświetlanie użytkowników którzy zaliczyli
myCtrls.controller('usersZal', ['$scope', '$http','checkToken', function($scope, $http, checkToken){		

			$http.post('api/admin/users/getZal', {
				token: checkToken.raw()
			}).success(function(data) {
				console.log("Dane pobrane.");
				$scope.users = data;
			}).	error(function(data){
				console.log("Błąd komunikacji z API");
			});


}]);

// Wyświetlanie użytkowników którzy nie zaliczyli
myCtrls.controller('getPass', ['$scope', '$http','checkToken', function($scope, $http, checkToken){		

			$http.post('api/admin/users/getPass', {
				token: checkToken.raw()
			}).success(function(data) {
				console.log("Dane pobrane.");
				$scope.users = data;
			}).	error(function(data){
				console.log("Błąd komunikacji z API");
			});


}]);

// Wyświetlanie użytkowników którzy nie rozpoczęli kursu (nie zalogowali się)
myCtrls.controller('getNoStart', ['$scope', '$http','checkToken', function($scope, $http, checkToken){		

			$http.post('api/admin/users/getNoStart', {
				token: checkToken.raw()
			}).success(function(data) {
				console.log("Dane pobrane.");
				$scope.users = data;
			}).	error(function(data){
				console.log("Błąd komunikacji z API");
			});


}]);

// Wyświetlanie wyników szczegółowych kursu
myCtrls.controller('resultCourse', ['$scope', '$http','checkToken', function($scope, $http, checkToken){		

			$http.post('api/admin/users/resultCourse', {
				token: checkToken.raw()
			}).success(function(data) {
				console.log("Dane pobrane.");
				$scope.users = data;
			}).	error(function(data){
				console.log("Błąd komunikacji z API");
			});


}]);

// Zgłoszenia problemów poprzez formularz
myCtrls.controller('problem', ['$scope', '$http','checkToken', function($scope, $http, checkToken){		

			$http.post('api/admin/users/problem', {
				token: checkToken.raw()
			}).success(function(data) {
				console.log("Dane pobrane.");
				$scope.users = data;
			}).	error(function(data){
				console.log("Błąd komunikacji z API");
			});


}]);

// Liczba wszystkich użytkowników
myCtrls.controller('statUsers', ['$scope', '$http','checkToken', function($scope, $http, checkToken){		

			$http.post('api/admin/users/statUsers', {
				token: checkToken.raw()
			}).success(function(data) {
				console.log("Dane pobrane.");
				$scope.users = data;
			}).	error(function(data){
				console.log("Błąd komunikacji z API");
			});


}]);

// Liczba użytkowników którzy zaliczyli
myCtrls.controller('statUsersZal', ['$scope', '$http','checkToken', function($scope, $http, checkToken){		

			$http.post('api/admin/users/statUsersZal', {
				token: checkToken.raw()
			}).success(function(data) {
				console.log("Dane pobrane.");
				$scope.users = data;
			}).	error(function(data){
				console.log("Błąd komunikacji z API");
			});


}]);

// Liczba użytkowników którzy nie zaliczyli
myCtrls.controller('statUsersNo', ['$scope', '$http','checkToken', function($scope, $http, checkToken){		

			$http.post('api/admin/users/statUsersNo', {
				token: checkToken.raw()
			}).success(function(data) {
				console.log("Dane pobrane.");
				$scope.users = data;
			}).	error(function(data){
				console.log("Błąd komunikacji z API");
			});


}]);
// Liczba użytkowników którzy nie rozpoczęli
myCtrls.controller('statUsersNoStart', ['$scope', '$http','checkToken', function($scope, $http, checkToken){		

			$http.post('api/admin/users/statUsersNoStart', {
				token: checkToken.raw()
			}).success(function(data) {
				console.log("Dane pobrane.");
				$scope.users = data;
			}).	error(function(data){
				console.log("Błąd komunikacji z API");
			});


}]);


// Edytowanie użytkowników  
myCtrls.controller('userEdit', ['$scope','$http','$stateParams',function($scope,$http, $stateParams){

			var userId = $stateParams.user_id;
			$scope.user_id = userId;

			$http.post('api/admin/users/get/'+userId).
			success(function(data) {
				$scope.user = data;
				console.log("User pobrany");
			}).error(function(data){
				console.log("Błąd pobrania danych");
			});

			$scope.saveChanges = function(user) {
					user = $scope.user;
					$http.post('api/admin/users/update/', {
							user : user,
							user_id : userId,
							username : user.username,
							password : user.password,
							firstname : user.firstname,
							lastname : user.lastname,
							email : user.email,
							firma : user.firma

					} ). success(function(errors) {

						$scope.submit = true;

						if (errors) {
							$scope.errors = errors;
						} else {
							$scope.user = {};
							$scope.errors = {};
							$scope.success = true;
							console.log(user);
						}


					}). error(function(data){
						console.log("Błąd edycji użytkownika");
					});
			};
}]);

//Dodawanie użytkowników 
myCtrls.controller('userCreate', ['$scope','$http','$filter',function($scope,$http,$filter){

			$scope.user = {};

			$scope.createUser = function(user) {

					user = $scope.user;

					$http.post('api/admin/users/create/', {

							user : user,
							username : user.username,
							password : user.password,
							firstname : user.firstname,
							lastname : user.lastname,
							email : user.email,
							firma : user.firma
							
					} ). success(function(errors) {

						$scope.submit = true;

						if (errors) {
							$scope.errors = errors;
						} else {
							$scope.user = {};
							$scope.errors = {};
							$scope.success = true;
							console.log(user.username);
						}


					}). error(function(data){
						console.log("Błąd dodania użytkownika");
					});
			};
}]);


// Logowanie admina
myCtrls.controller( 'login' , [ '$scope' , '$http' , 'store', 'checkToken', '$location', function( $scope , $http , store, checkToken, $location){

	if (checkToken.loggedIn())
		// $location.path('/dashboard');
		//window.location.href = '/integrity/admin/#/dashboard.html';
		window.location.href = '/szkoleniowiec/admin/';
	
	$scope.user = {};

	$scope.formSubmit = function ( user ) {

		$http.post( 'api/admin/users/login/' , {
			adminname : user.adminname,
			password : user.password
		}).success( function( data ){

			$scope.submit = true;
			$scope.error = data.error;
			
			if ( !data.error )
			{
				store.set( 'token' , data.token );
				location.reload();
				//console.log(user.adminname);
			
			}
			
		}).error( function(){
			console.log( 'Błąd połączenia z API' );
		});

	};

	console.log( checkToken.payload().username );
	
}]);

// ---------------------- GRUPY  ----------------------------------------------------------------

// Grupy wyświetlanie
myCtrls.controller('groups', ['$scope', '$http','checkToken', function($scope, $http, checkToken){		

			$http.post('api/admin/groups/get', {
				token: checkToken.raw()
			}).success(function(data) {
				$scope.groups = data;
			}).	error(function(data){
				console.log("Błąd komunikacji z API");
			});

}]);

//Dodawanie grupy 
myCtrls.controller('groupCreate', ['$scope','$http','$filter',function($scope,$http,$filter){

			$scope.group = {};

			$scope.createGroup = function(group) {

					group = $scope.group;

					$http.post('api/admin/groups/create/', {

							group : group,
							nazwa : group.nazwa,
							organizacja : group.organizacja,
							opis : group.opis
							
					} ). success(function(errors) {

						$scope.submit = true;

						if (errors) {
							$scope.errors = errors;
						} else {
							$scope.group = {};
							$scope.errors = {};
							$scope.success = true;
							console.log(group.nazwa);
						}


					}). error(function(data){
						console.log("Błąd dodania użytkownika");
					});
			};
}]);

// Edytowanie użytkowników  
myCtrls.controller('groupEdit', ['$scope','$http','$stateParams',function($scope,$http, $stateParams){

			var groupId = $stateParams.group_id;
			$scope.group_id = groupId;

			$http.post('api/admin/groups/get/'+groupId).
			success(function(data) {
				$scope.group = data;
				console.log("Grupa pobrana");
			}).error(function(data){
				console.log("Błąd pobrania danych");
			});

			$scope.saveChanges = function(group) {
					group = $scope.group;
					$http.post('api/admin/groups/update/', {
							group : group,
							group_id : groupId,
							nazwa : group.nazwa,
							organizacja : group.organizacja,
							opis : group.opis

					} ). success(function(errors) {

						$scope.submit = true;

						if (errors) {
							$scope.errors = errors;
						} else {
							$scope.group = {};
							$scope.errors = {};
							$scope.success = true;
							console.log(group);
						}


					}). error(function(data){
						console.log("Błąd edycji użytkownika");
					});
			};
}]);

// ---------------------- KURSY  ----------------------------------------------------------------

// Kursy wyświetlanie
myCtrls.controller('courses', ['$scope', '$http','checkToken', function($scope, $http, checkToken){		

			$http.post('api/admin/courses/get', {
				token: checkToken.raw()
			}).success(function(data) {
				$scope.courses = data;
			}).	error(function(data){
				console.log("Błąd komunikacji z API");
			});

}]);

//Dodawanie kursu 
myCtrls.controller('courseCreate', ['$scope','$http','$filter',function($scope,$http,$filter){

			$scope.course = {};

			$scope.createCourse = function(course) {

					course = $scope.course;

					$http.post('api/admin/courses/create/', {

							course : course,
							nazwa : course.nazwa,
							opis : course.opis
							
					} ). success(function(errors) {

						$scope.submit = true;

						if (errors) {
							$scope.errors = errors;
						} else {
							$scope.course = {};
							$scope.errors = {};
							$scope.success = true;
							console.log(course.nazwa);
						}


					}). error(function(data){
						console.log("Błąd dodania użytkownika");
					});
			};
}]);

// Edytowanie kursów  
myCtrls.controller('courseEdit', ['$scope','$http','$stateParams',function($scope,$http, $stateParams){

			var courseId = $stateParams.course_id;
			$scope.course_id = courseId;

			$http.post('api/admin/courses/get/'+courseId).
			success(function(data) {
				$scope.course = data;
				console.log("Grupa pobrana");
			}).error(function(data){
				console.log("Błąd pobrania danych");
			});

			$scope.saveChanges = function(course) {
					course = $scope.course;
					$http.post('api/admin/courses/update/', {
							course : course,
							course_id : courseId,
							nazwa : course.nazwa,
							opis : course.opis

					} ). success(function(errors) {

						$scope.submit = true;

						if (errors) {
							$scope.errors = errors;
						} else {
							$scope.course = {};
							$scope.errors = {};
							$scope.success = true;
							console.log(course);
						}


					}). error(function(data){
						console.log("Błąd edycji użytkownika");
					});
			};
}]);



