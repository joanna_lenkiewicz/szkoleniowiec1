var app = angular.module( 'app' , [ 'ngRoute', 'angular-storage', 'angular-jwt','controllersSite', 'myServices','ngCookies' ] );

app.config( [ '$routeProvider' , '$httpProvider' , function( $routeProvider , $httpProvider ) {


	//============== Home =======================

	$routeProvider.otherwise ({
		redirectTo: '/index'
	});

	//============== Start =======================

	$routeProvider.when ('/index', {
		controller: 'navigation',
		templateUrl: 'views/courses.html'
	});

	//============== Raporty =======================

	$routeProvider.when ('/raporty', {
		controller: 'userPass',
		templateUrl: 'views/raport.php'
	});


	//============== Profil =======================


	$routeProvider.when ('/profil', {
		controller: 'userEdit',
		templateUrl: 'views/user-edit.html'
	});

	//============== Pomoc =======================


	$routeProvider.when ('/pomoc', {
		templateUrl: 'views/pomoc.html'
	});


	//============== Login =======================

	$routeProvider.when ('/login', {
		controller: 'login',
		templateUrl: 'login.html'
	});


}]);
