var controllersSite = angular.module( 'controllersSite' , [ 'ngRoute' ] );

// Nawigacja
controllersSite.controller('navigation', ['$scope', '$http' , 'store', '$location','checkToken','jwtHelper','$cookies', function($scope, $http , store, $location,checkToken,jwtHelper, $cookies ){

		if (!checkToken.loggedIn()) {
				$scope.loggedIn=false;
				window.location.href = '/szkoleniowiec/login.html';
			} else {
				$scope.loggedIn=true;
			};

		// Tworzenie ciasteczka
		var token = store.get ('token');
		token = jwtHelper.decodeToken(token);
		var tokenId = token.user_id;
		var tokenUsername = token.username;
		$cookies.put('user_id', tokenId);
		$cookies.put('username', tokenUsername);
		$scope.user_id = tokenId;
		$scope.username = tokenUsername;

		// Wylogowanie, usunięcie ciasteczka i tokena
		$scope.logout = function() {
				checkToken.del();
				location.reload();
				$cookies.remove('user_id');
				$cookies.remove('username');
				console.log("Wylogowano");
			};

}]);

// Wyświetlanie użytkowników
controllersSite.controller('users', ['$scope', '$http','checkToken', function($scope, $http, checkToken){

			$http.post('api/site/users/get', {
				token: checkToken.raw()
			}).success(function(data) {
				$scope.users = data;
				console.log("Dane pobrane.");
			}).	error(function(data){
				console.log("Błąd komunikacji z API");
			});

}]);

// Edytowanie użytkownika
controllersSite.controller('userEdit', ['$scope','$http','$routeParams','checkToken','jwtHelper','store',function($scope,$http,$routeParams,checkToken,jwtHelper,store){

			var token = store.get ('token');
			token = jwtHelper.decodeToken(token);
			var user_id = token.user_id;
			$scope.user_id = user_id;

			$http.get('api/site/users/get/'+user_id).
			success(function(data) {
				$scope.user = data;
				console.log("Pobrano dane");
			}).error(function(data){
				console.log("Błąd pobrania danych");
			});

			$scope.saveChanges = function(user) {
					user = $scope.user;
					$http.post('api/site/users/update/', {
							user : user,
							user_id : user_id,
							username : user.username,
							password : user.password,
							firstname : user.firstname,
							lastname : user.lastname,
							email : user.email,
							firma : user.firma

					} ). success(function(errors) {

						$scope.submit = true;

						if (errors) {
							$scope.errors = errors;
						} else {
							$scope.errors = {};
							$scope.success = true;
							console.log(user);
						}


					}). error(function(data){
						console.log("Błąd edycji użytkownika");
					});
			};

}]);


// Logowanie usera
controllersSite.controller( 'login' , [ '$scope' , '$http' , 'store', 'checkToken', '$location','$timeout', function( $scope , $http , store, checkToken, $location, $timeout){

	if (checkToken.loggedIn())
		//window.location.href = 'http://platforma.szkoleniowiec.poznan.pl';
		window.location.href = 'index.html';

	$scope.user = {};


	$scope.formSubmit = function ( user ) {

		$http.post( 'api/site/loger/login/' , {
			username : user.username,
			password : user.password
		}).success( function( data ){

			$scope.submit = true;

			if ( !data.error )
			{
				store.set( 'token' , data.token );
				location.reload();
			} else {
				$scope.error = data.error
			}

		}).error( function(){
			console.log( 'Błąd logowania' );
		});

	};

	console.log( checkToken.payload().email );

}]);

// Obliczanie statusu zaliczenia
controllersSite.controller('userPass', ['$scope','$http','$routeParams','checkToken','jwtHelper','store',function($scope,$http,$routeParams,checkToken,jwtHelper,store){

			var token = store.get ('token');
			token = jwtHelper.decodeToken(token);
			var user_id = token.user_id;
			$scope.user_id = user_id;

			$http.get('api/site/users/get/'+user_id).
			success(function(data) {
				$scope.user = data;
				console.log("Pobrano dane");
			}).error(function(data){
				console.log("Błąd pobrania danych");
			});


			$http.post('api/site/users/getPass', {
				token: checkToken.raw()
			}).success(function(data) {
				$scope.pass = data;
				$scope.pass.prog = data.prog;
				console.log("Próg zaliczenia pobrany.");
			}).	error(function(){
				console.log("Błąd komunikacji z API");
			});

			$http.get('api/site/users/coursePoints/'+user_id).
			success(function(data) {
				$scope.coursePoints = data;
				if (data.a1 > 0) {
					$scope.coursePointsA1='zaliczony';
				} else {
					$scope.coursePointsA1='niezaliczony';
				}
				if (data.c1 > 0) {
					$scope.coursePointsC1='zaliczony';
				} else {
					$scope.coursePointsC1='niezaliczony';
				}
				if (data.e1 > 0) {
					$scope.coursePointsE1='zaliczony';
				} else {
					$scope.coursePointsE1='niezaliczony';
				}
				if (data.g1 > 0) {
					$scope.coursePointsG1='zaliczony';
				} else {
					$scope.coursePointsG1='niezaliczony';
				}
				if (data.h1 > 0) {
					$scope.coursePointsH1='zaliczony';
				} else {
					$scope.coursePointsH1='niezaliczony';
				}
				if (data.i1 > 0) {
					$scope.coursePointsI1='zaliczony';
				} else {
					$scope.coursePointsI1='niezaliczony';
				}
				console.log("Pobrano dane zaliczenia kursów");
			}).error(function(data){
				console.log("Błąd pobrania danych");
			});

			$http.get('api/site/users/testPoints/'+user_id).
			success(function(data) {
				$scope.testpoints = data;
				if ((data.b1+data.b2+data.b3+data.b4+data.b5+data.b6+data.b7+data.b8+data.b9+data.b10) > (((data.b1+data.b2+data.b3+data.b4+data.b5+data.b6+data.b7+data.b8+data.b9+data.b10)*$scope.pass.prog)/100)) {
					$scope.testPointsB='zaliczony';
				} else {
					$scope.testPointsB='niezaliczony';
				}
				if ((data.d1+data.d2+data.d3+data.d4+data.d5) > (((data.d1+data.d2+data.d3+data.d4+data.d5)*$scope.pass.prog)/100)) {
					$scope.testPointsD='zaliczony';
				} else {
					$scope.testPointsD='niezaliczony';
				}
				if ((data.f1+data.f2+data.f3+data.f4+data.f5) > (((data.f1+data.f2+data.f3+data.f4+data.f5)*$scope.pass.prog)/100)) {
					$scope.testPointsF='zaliczony';
				} else {
					$scope.testPointsF='niezaliczony';
				}
				console.log("Pobrano dane zaliczenia testów");
			}).error(function(data){
				console.log("Błąd pobrania danych");
			});



}]);
