<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$post = file_get_contents( 'php://input' );
		$_POST = json_decode( $post , true );

		$this->load->model( 'site/Users_model' );

	}

	public function get( $user_id = false )
	{	
		$output = $this->Users_model->get( $user_id );
		echo json_encode( $output );	
	}
	

	public function update()
	{
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('username','Nazwa użytkownika','required|min_length[4]');
		$this->form_validation->set_rules('password','Nowe hasło','required|min_length[8]');
		$this->form_validation->set_rules('firstname','Imię','required|min_length[3]');
		$this->form_validation->set_rules('lastname','Nazwisko','required|min_length[3]');
		$this->form_validation->set_rules('email','E-mail','required|valid_email|callback_unique_email');
		$this->form_validation->set_rules('firma','Organizacja','required|min_length[2]');
		

		if ($this->form_validation->run()) {
			$user = $this->input->post( 'user' );

			$user['password'] = crypt($user['password'], config_item('encryption_key'));

			$this->Users_model->update( $user );
		} else {
			$errors['username'] = form_error('username');
			$errors['password'] = form_error('password');
			$errors['firstname'] = form_error('firstname');
			$errors['lastname'] = form_error('lastname');
			$errors['email'] = form_error('email');
			$errors['firma'] = form_error('firma');
			
			echo json_encode($errors);
		}
	}

	public function create()
	{
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('username','Nazwa użytkownika','required|min_length[4]');
		$this->form_validation->set_rules('password','Nowe hasło','required|min_length[8]');
		$this->form_validation->set_rules('firstname','Imię','required|min_length[3]');
		$this->form_validation->set_rules('lastname','Nazwisko','required|min_length[3]');
		$this->form_validation->set_rules('email','E-mail','required|valid_email|callback_unique_email');
		$this->form_validation->set_rules('firma','Organizacja','required|min_length[2]');

		if ($this->form_validation->run()) {
			$user = $this->input->post( 'user' );
			
			$user['password'] = crypt($user['password'], config_item('encryption_key'));
			
			$this->Users_model->create( $user );
		} else {
			$errors['username'] = form_error('username');
			$errors['password'] = form_error('password');
			$errors['firstname'] = form_error('firstname');
			$errors['lastname'] = form_error('lastname');
			$errors['email'] = form_error('email');
			$errors['firma'] = form_error('firma');

			echo json_encode($errors);
		}

	}


	function unique_email() 
	{
		$user_id = $this->input->post('user_id');
		$email = $this->input->post('email');

		if ($this->Users_model->get_unique($user_id, $email))
		{
			$this->form_validation->set_message('unique_email','Inny użytkownik ma taki email');
			return false;
		} else {
			return true;
		}
	}


	public function getPass()
	{	
		$output = $this->Users_model->getPass();
		echo json_encode( $output );	
	}

	public function coursePoints($user_id)
	{	
		$output = $this->Users_model->coursePoints( $user_id );
		echo json_encode( $output );	
	}

	public function testPoints($user_id)
	{	
		$output = $this->Users_model->testPoints( $user_id );
		echo json_encode( $output );	
	}

}
