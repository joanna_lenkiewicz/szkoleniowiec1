<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Loger extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$post = file_get_contents( 'php://input' );
		$_POST = json_decode( $post , true );

		$this->load->model( 'site/Loger_model' );

	}

	public function login()
	{
		$username = $this->input->post( 'username' );
		$password = $this->input->post( 'password' );
		$password = crypt( $password , config_item( 'encryption_key' ) );

		$login = $this->Loger_model->login( $username , $password );

		if ( !$login )
		{
			$output['error'] = 'Błędne hasło lub login';
		}
		else
		{
			$token = $this->jwt->encode( array(
				'user_id' => $login->user_id,
				'username' => $login->username,
				'firstname' => $login->firstname,
				'lastname' => $login->lastname,
				'email' => $login->email
				) , config_item( 'encryption_key' ) );

			$output['token'] = $token;
		}

		echo json_encode( $output );

	}


}
