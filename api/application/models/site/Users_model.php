<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends CI_Model {

	public function get( $user_id = false )
	{

		if ( $user_id == false )
		{
			$q = $this->db->get( 'users' );
			$q = $q->result();
		}
		else
		{
			$this->db->where( 'user_id' , $user_id );
			$q = $this->db->get( 'users' );
			$q = $q->row();
		}

		return $q;

	}

	public function update( $user )
	{
		$this->db->where( 'user_id' , $user['user_id'] );
		$this->db->update( 'users' , $user );
	}

	public function create( $user )
	{
		$this->db->insert( 'users' , $user );
	}


	public function get_unique($user_id, $email) 
	{
		$this->db->where('email', $email);
		!$user_id || $this->db->where('user_id !=', $user_id);
		$q = $this->db->get('users');
		return $q->row();
	}

	public function register( $user )
	{
		$this->db->insert( 'users' , $user );
	}
	

	public function getPass()
	{
		{
			$this->db->where( 'kurs_id' , 1 );
			$q = $this->db->get( 'configurations' );
			$q = $q->row();
		}
		return $q;

	}

	public function coursePoints($user_id)
	{
		{
			$this->db->where(  'user_id' , $user_id  );
			$q = $this->db->get( 'wyniki_kursy' );
			$q = $q->row();
		}
		return $q;

	}

	public function testPoints($user_id)
	{
		{
			$this->db->where(  'user_id' , $user_id  );
			$q = $this->db->get( 'wyniki' );
			$q = $q->row();
		}
		return $q;

	}



}