<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Loger_model extends CI_Model {

	public function login( $username , $password )
	{
		$this->db->where( 'username' , $username );
		$q = $this->db->get( 'users' );
		$result = $q->row();

		if ( empty( $result ) || $password != $result->password )
		{
			$output = false;
		}
		else
		{
			$output = $result;
		}

		return $output;

	}

}